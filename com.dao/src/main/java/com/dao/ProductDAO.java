package com.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entity.Product;

import java.util.List;


@Repository
public interface ProductDAO extends CrudRepository<Product,Integer>{
	
	public List<Product> findByProductCategory(String productCategory);

	public List<Product> findByProductName(String productName);
	
	public List<Product> findByProductCategoryStartingWithIgnoreCaseOrProductNameContainingIgnoreCase(String productCategory, String productName);
	
	public List<Product> findByProductNameContainingIgnoreCase(String productName);
	

}
