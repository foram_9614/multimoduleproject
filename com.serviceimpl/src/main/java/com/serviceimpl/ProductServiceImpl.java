package com.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.ProductDAO;
import com.entity.Product;
import com.serviceapi.ProductService;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {
	
	private ProductDAO productDao;

    public ProductServiceImpl(ProductDAO productDao){
        this.productDao = productDao;
    }
    

	@Override
	public List<Product> search(String productCategory, String productName) {
		// TODO Auto-generated method stub
		List<Product> products = productDao.findByProductCategoryStartingWithIgnoreCaseOrProductNameContainingIgnoreCase(productCategory, productName);
		return products;
	}

	@Override
	public Product checkProduct(int productID) {
		// TODO Auto-generated method stub
		return productDao.findOne(productID);
	}

	@Override
	public List<Product> getAllProducts() {
		List<Product> products = (List<Product>) productDao.findAll();
		return products;
	}

	public void addProduct(Product product) {
		// TODO Auto-generated method stub
		productDao.save(product);
	}

	@Override
	public void buyProduct(int productId, int quantity) {
		// TODO Auto-generated method stub
		Product product = productDao.findOne(productId);
		int productQuantity = product.getProductQuantity();
		product.setProductQuantity(productQuantity - quantity);
		productDao.save(product);

	}

	@Override
	public List<Product> searchByName(String productName) {
		// TODO Auto-generated method stub
		List<Product> products = productDao.findByProductNameContainingIgnoreCase(productName);
		return products;
	}

}
