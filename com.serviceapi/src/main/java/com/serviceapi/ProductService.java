package com.serviceapi;

import java.util.List;

import com.entity.Product;

public interface ProductService {

	List<Product> getAllProducts();

	List<Product> search(String productCategory, String productName);

	Product checkProduct(int productID);

	void buyProduct(int productId, int quantity);

	void addProduct(Product product);

	List<Product> searchByName(String productName);

}
