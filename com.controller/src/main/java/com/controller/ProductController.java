package com.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Product;
import com.serviceapi.ProductService;
import com.serviceimpl.ProductServiceImpl;

@RestController
public class ProductController {

	@RequestMapping("/")
	public String Hello() {
		System.out.println("Yeyy");
		return "Hello";
	}

	// @Autowired
	// ProductService productService;

	private ProductServiceImpl productService;

	public ProductController(ProductServiceImpl productService){
        this.productService = productService;
	}
    
    @RequestMapping("/search/{productCategory}")
    public List<Product> searchProduct(@PathVariable("productCategory") String productCategory){
        System.out.println(productCategory);
        List<Product> productVOs = productService.search(productCategory, productCategory);
        
        return productVOs;
    }
    
    @RequestMapping("/searchByProduct/{productName}")
    public List<Product> searchByName(@PathVariable("productName") String productName){
        List<Product> productVOs = productService.searchByName(productName);
        return productVOs;
    }
    
//    @RequestMapping("/addProductForm")
//    public String addProductForm(Model model){
//        Product product = new Product();
//        model.addAttribute("product", product);
//        return "addProductForm";
//    }
//    
//    @RequestMapping("/addProduct")
//    public String addProduct(@ModelAttribute("product") Product product){
//        productService.addProduct(product);
//        return "success";
//    }
    
    @RequestMapping("/getAllProducts")
    public List<Product> getProducts(){
        List<Product> productList = productService.getAllProducts();
        if (productList.size() > 0) {
        	  Collections.sort(productList, new Comparator<Product>() {
        	      @Override
        	      public int compare(Product product1, Product product2) {
        	    	  if(product1.getProductCategory().equals(product2.getProductCategory()))
        	    	  {
        	    		  return Double.compare(product1.getProductPrice(), product2.getProductPrice());
        	    	  }
        	          return product1.getProductCategory().compareTo(product2.getProductCategory());
        	      }
        	  });
        	}
        
        return productList;
    }
    
    @RequestMapping("/checkQuantity/{productId}/{quantity}")
    public boolean checkProduct(@PathVariable("productId") Integer productID, @PathVariable("quantity") Integer quantity){
        Product product = productService.checkProduct(productID);
        if(product.getProductQuantity()>=quantity&& quantity>0){
            return true;
        }
        return false;
    }
    
    @RequestMapping("/getProductById/{productId}")
    public Product getProduct(@PathVariable("productId") Integer productID){
        Product product = productService.checkProduct(productID);
        return product;
    }
    
    
}